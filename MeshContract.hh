/*===========================================================================*\
* This plugin uses the framework of  the Smoother Plugin shipped with OpenFlipper*
\*===========================================================================*/




#ifndef MeshContract_HH
#define MeshContract_HH

#include <OpenFlipper/BasePlugin/BaseInterface.hh>
#include <OpenFlipper/BasePlugin/ToolboxInterface.hh>
#include <OpenFlipper/BasePlugin/LoggingInterface.hh>
#include <OpenFlipper/BasePlugin/ScriptInterface.hh>
#include <OpenFlipper/BasePlugin/BackupInterface.hh>

#include <OpenFlipper/BasePlugin/MouseInterface.hh>
#include <OpenFlipper/BasePlugin/PickingInterface.hh>
#include <OpenFlipper/common/Types.hh>
#include <ObjectTypes/TriangleMesh/TriangleMesh.hh>

#include <ACG/Scenegraph/LineNode.hh>

#include <ObjectTypes/PolyMesh/PolyMesh.hh>
#include <OpenMesh/Core/Utils/Property.hh>
#include <OpenMesh/Tools/Utils/HeapT.hh>
using namespace OpenMesh;
class MeshContract : public QObject, BaseInterface, ToolboxInterface, LoggingInterface, ScriptInterface, BackupInterface, MouseInterface, PickingInterface
{
  Q_OBJECT
  Q_INTERFACES(BaseInterface)
  Q_INTERFACES(ToolboxInterface)
  Q_INTERFACES(LoggingInterface)
  Q_INTERFACES(ScriptInterface)
  Q_INTERFACES(BackupInterface)
  Q_INTERFACES(MouseInterface)
  signals:
    //BaseInterface
    void updateView();
    void updatedObject(int _id, const UpdateType& _type);
    void setSlotDescription(QString     _slotName,   QString     _slotDescription,
                          QStringList _parameters, QStringList _descriptions);

    //LoggingInterface
    void log(Logtype _type, QString _message);
    void log(QString _message);
    
    // ToolboxInterface
    void addToolbox( QString _name  , QWidget* _widget, QIcon* _icon);
    
	void addEmptyObject( DataType _type, int& _id);   
    // ScriptInterface
    void scriptInfo(QString _functionName);
    
    // BackupInterface
    void createBackup( int _id , QString _name, UpdateType _type = UPDATE_ALL );

  public:

/// Heap interface
  class HeapInterface
  {
  public:

	  HeapInterface(TriMesh& _mesh,
      HPropHandleT<double> _prio,
      HPropHandleT<int>   _pos)
      : mesh_(_mesh), prio_(_prio), pos_(_pos)
    { }

    inline bool
    less( HalfedgeHandle _h0, HalfedgeHandle _h1 )
    { return mesh_.property(prio_, _h0) < mesh_.property(prio_, _h1); }

    inline bool
    greater( HalfedgeHandle _vh0, HalfedgeHandle _vh1 )
    { return mesh_.property(prio_, _vh0) > mesh_.property(prio_, _vh1); }

    inline int
    get_heap_position(HalfedgeHandle _vh)
    { return mesh_.property(pos_, _vh); }

    inline void
    set_heap_position(HalfedgeHandle _vh, int _pos)
    { mesh_.property(pos_, _vh) = _pos; }


  private:
		TriMesh&              mesh_;
		HPropHandleT<double>  prio_;
		HPropHandleT<int>     pos_;
  };

  typedef Utils::HeapT<HalfedgeHandle, HeapInterface>  SkelHeap;

	
//Heap stuff end
    MeshContract();
    ~MeshContract();

    // BaseInterface
    QString name() { return (QString("Simple Smoother")); };
    QString description( ) { return (QString("Smooths the active Mesh")); };

   private:
	SkelHeap *theHeap;   
	double Wh0;						// Initial weight for "Attraction"
	double Wl0;						// Initial weight for "Contraction"
	double Sl;						// Contraction "Scale" factor
	double avgArea;					// Average face area of the model 
	bool   anchorSelectionMode;		// selecting anchor points ?
	TriMesh::VertexHandle lastPickedVertex; // for anchor Selection
	QPoint	lastPickedPos;
	VPropHandleT<TriMesh::Point> anchorPoints;
    QSpinBox* iterationsSpinbox_;	// No. of iterations
	QDoubleSpinBox* spinBoxWh0;		// Initial "Attraction" value spinbox
	QDoubleSpinBox* spinBoxWl0;		// Initial "Contraction" value spinbox
	QDoubleSpinBox* spinBoxSl;		// Contraction "Scale" factor spinbox
	QCheckBox*	anchorCheck;
	QDoubleSpinBox* vx;
	QDoubleSpinBox* vy;
	QDoubleSpinBox* vz;
	  // Line Node
	std::vector<ACG::SceneGraph::LineNode*> lineNodes_;
	/*
	* Sum-area-all-model-faces/no-of-model-faces
	* For the same object, model with higher triangle count has lower avg face area
	* Use this to configure initial contraction factor
	*/
	double findAvgFaceArea(TriMesh *mesh);	

	/*
	* Area of all faces incident on vertex vh
	* Use this to see "how much" contraction has taken place already
	* Scale attraction factor accordingly
	*/
	double findOneRingArea(TriMesh *mesh, TriMesh::VertexHandle vh);

	/*
	* Volume enclosed by mesh
	* Terminate iterations when volume < 1e-6
	*/
	double findMeshVolume(TriMesh *mesh);

	/*
	* return area of face fh
	*/
	double faceArea(TriMesh *mesh, TriMesh::FaceHandle fh);

	/*
	* for all Edges of mesh, compute the cotangent weight cot(a)+cot(b)
	* for each edge e(v0,v1) 
	* set weight(e)   = cot(a)+cot(b)
	*	  weight(v0) += cot(a)+cot(b)
	*	  weight(v1) += cot(a)+cot(b)
	*
	* if(cot(a)+cot(b) < threshold) set weight=threshold
	*/
	void	computeWeights(TriMesh *mesh);
	/*
	* check for degenerate cases
	*/

	void	handleDegenerate(TriMesh *mesh);


	bool	okToCollapse(TriMesh *mesh, HalfedgeHandle heh);

	int		doCollapse(TriMesh *mesh, HalfedgeHandle heh);

	
   private slots:
    void simpleLaplace();
	void simpleCollapse();
    void simpleCenter();
	void simpleColor();
	void checkAnchorMode();
    void initializePlugin(); // BaseInterface
    void transferValues();
  
    void slotMouseEvent( QMouseEvent* _event );  // MouseInterface
    void pluginsInitialized(); // BaseInterface
    
    // Scriptable functions
   public slots:
       
    void simpleLaplace(int _iterations);
       
    QString version() { return QString("1.0"); };
};

#endif //MeshContract_HH
