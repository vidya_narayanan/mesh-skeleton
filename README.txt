Skeleton Extraction by Mesh Contraction
-----------------------------------------

Done as a part of the Graphics Course(http://drona.csa.iisc.ernet.in/~vijayn/courses/Graphics/index.html)

Based on the paper 'Skeleton extraction by mesh contraction', siggraph 08 (see http://visgraph.cse.ust.hk/projects/skeleton/ )
this project implements a framework to skeletonize triangulated surface meshes by first contracting them into zero volume meshes 
and then collapsing this contracted mesh into a skeleton.

Post processing steps such as node centering have not been implemented.

The original paper uses quadric error metrics(QEM) for collapsing triangle edges, this did not give the desired results,
what worked better was a simpler greedy 'collapse shortest edge first' approach.

Demo videos - https://www.youtube.com/playlist?list=PLAV6p51tONOvs9MWAQpnws4-Z5HtgvQ4w

The demo videos and project report is available in the 'Videos and Reports' folder.

This was built as a plugin to OpenFlipper(which is build atop OpenMesh) using OpenNL and SuperLU linear solver.

Also See http://www.openflipper.org/media/Documentation/OpenFlipper-1.0-RC6/ex1.html for building the plugin