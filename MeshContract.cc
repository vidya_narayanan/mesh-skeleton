/*===========================================================================*\
* This plugin uses the framework of  the Smoother Plugin shipped with OpenFlipper*
\*===========================================================================*/

// How I need to find the new position for each vertex:
			// Solve: (least square)  n = no. of vertices
			// Ax = b  A=2n*n matrix , x = n*1 new positions, b = 2n*1  
			// A =[Wl*L] 
			//	  [Wh*I]
			// b = [0]
			//	   [Wh*V]
			// Wl = diagonal weighting matrix for contraction
			// Wh = diagonal weighting matrix for attraction
			// L = Laplace curvature flow operator (cotangent weighting scheme..)
			// L(ij) = cot(aij) + cot(bij) if (i,j) in E
			//       = -(sum of weights of edges in the 1-ring) if (i=j) i.e the diagonal
			//		 = 0 otherwise
			// this is a sparse matrix but has entries in other than the diagonal also...
			// Note: LV = 0 => removing the normal components in the mesh and contracting the mesh..

			// The above is an iterative process
			// updating Wl(i+1) = s*Wl(i) , s = 2.0
			// updating Wh(i+1) = Wh(i)*(sqrt(original-area / ith-iteration-area)) area == one-ring-area

			// Initial values:
			// Wh = 1.0 Wl = 10^-3*(sqrt(Avg-face-area))

			// Final Condition: 
			// The iteration stops when 
			// current-mesh-volume/original-mesh-volume < 1e-6


#include "MeshContract.hh"
#include <ObjectTypes\Knotvector\KnotvectorT.hh>
#include <ObjectTypes\PolyLine\PolyLine.hh>
#include <OpenMesh\Core\Geometry\QuadricT.hh>
#include <OpenMesh\Core\Geometry\VectorT.hh>
#include "OpenFlipper/BasePlugin/PluginFunctions.hh"
// Numerical Library 
#include "openNL/NL/nl.h"
#define _ITERATOR_DEBUG_LEVEL 0

#define BOUNDS 1.0
#define EDGEBOUNDS 200000.0
#define MIN_COT_WT 0.000000001			// Min-Threshold value for cotangent weights, weights should not get negative
#define MIN_DEGENERATE_FACE 0.00000001	// Min-Threshold value for identifying degenerate face, pull them out of the equation
#define MAX_CONTR 10000				// Max-Threshold value for contraction weights
#define MAX_ATTR 10000				// Max-Threshold value for attraction weights
#define INV -999
#define DEL 0.001
MeshContract::MeshContract() :
        iterationsSpinbox_(0)
{

}

MeshContract::~MeshContract()
{

}

void MeshContract::initializePlugin()
{
   // Create the Toolbox Widget
   QWidget* toolBox = new QWidget();
   QGridLayout* layout = new QGridLayout(toolBox);

   
   QPushButton* colorButton = new QPushButton("&Color",toolBox);
   colorButton->setToolTip(tr("Color the mesh according to the computed cot weights"));
   colorButton->setWhatsThis(tr("Color the mesh"));


   QPushButton* contractButton = new QPushButton("&Contract",toolBox);
   contractButton->setToolTip(tr("Contracts an Object using Laplacian Smoothing with cotangent weighting and vertices as attraction points."));
   contractButton->setWhatsThis(tr("Contracts an Object using Laplacian Smoothing with cotangent weighting and vertices as attraction points"));

   
   QPushButton* collapseButton = new QPushButton("&Collapse",toolBox);
   collapseButton->setToolTip(tr("Performs edge collapse using QEM."));
   collapseButton->setWhatsThis(tr("Performs edge collapse using QEM to convert mesh to 1d skeleton"));

   
   QPushButton* centerButton = new QPushButton("&Center",toolBox);
   collapseButton->setToolTip(tr("Performs node centering."));
   collapseButton->setWhatsThis(tr("Performs node centering"));

   anchorCheck = new QCheckBox(toolBox);
   anchorCheck->setToolTip(tr("In anchoring mode"));
   anchorCheck->setWhatsThis(tr("The number of the smooting operations."));
   

   iterationsSpinbox_ =  new QSpinBox(toolBox) ;
   iterationsSpinbox_->setMinimum(1);
   iterationsSpinbox_->setMaximum(100000);
   iterationsSpinbox_->setSingleStep(1);
   iterationsSpinbox_->setToolTip(tr("The number of the smooting operations."));
   iterationsSpinbox_->setWhatsThis(tr("Give the number, how often the Laplacian Smoothing should modify the object."));

   spinBoxWh0 = new QDoubleSpinBox(toolBox);
   spinBoxWh0->setMinimum(0.1);
   spinBoxWh0->setMaximum(100);
   spinBoxWh0->setSingleStep(0.1);
   spinBoxWh0->setToolTip(tr("Initial value for attraction weight"));
   spinBoxWh0->setWhatsThis(tr("Initial value for attr weight"));

   spinBoxWl0 = new QDoubleSpinBox(toolBox);
   spinBoxWl0->setMinimum(0.1);
   spinBoxWl0->setMaximum(100);
   spinBoxWl0->setSingleStep(0.1);
   spinBoxWl0->setToolTip(tr("Initial value for contraction weight"));
   spinBoxWl0->setWhatsThis(tr("Initial value for contraction weight"));

   spinBoxSl = new QDoubleSpinBox(toolBox);
   spinBoxSl->setMinimum(0);
   spinBoxSl->setMaximum(100);
   spinBoxSl->setSingleStep(0.01);
   spinBoxSl->setToolTip(tr("contraction scaling factor"));
   spinBoxSl->setWhatsThis(tr("contraction scaling factor"));

   vx = new QDoubleSpinBox(toolBox);
   vx->setSingleStep(0.00001);
   vx->setDecimals(15);
   
   vy = new QDoubleSpinBox(toolBox);
   vy->setSingleStep(0.00001);
   vy->setDecimals(15);
   
   vz = new QDoubleSpinBox(toolBox);
   vz->setSingleStep(0.00001);
   vz->setDecimals(15);
   QPushButton* transfer = new QPushButton("&Transfer",toolBox);
 
   QLabel* label = new QLabel("Iterations:");
   QLabel* labelWh0 = new QLabel("Attraction Weight: ");
   QLabel* labelWl0 = new QLabel("Contraction Weight: ");
   QLabel* labelSl = new QLabel("Contraction Scale Factor: ");
   QLabel* labelAnchor = new QLabel("Select Anchor Mode: ");
   layout->addWidget( label             , 0, 0);  
   layout->addWidget( iterationsSpinbox_, 0, 1);
   layout->addWidget( labelWh0				, 1, 0);
   layout->addWidget( spinBoxWh0			, 1, 1);
   layout->addWidget( labelWl0				, 2, 0);  
   layout->addWidget( spinBoxWl0			, 2, 1);
   layout->addWidget( labelSl				, 3, 0);   
   layout->addWidget( spinBoxSl				, 3, 1);
   layout->addWidget( colorButton		, 4, 0);
   layout->addWidget( contractButton    , 4, 1);
   layout->addWidget( collapseButton	, 5, 1);
   layout->addWidget( centerButton		, 6, 1);
   layout->addWidget( labelAnchor		, 7, 0);
   layout->addWidget( anchorCheck		, 7, 1);
   layout->addWidget( vx				, 8, 0);
   layout->addWidget(vy,				8, 1);
   layout->addWidget(vz,				8, 2);
   layout->addWidget(transfer,		7,2);
   layout->addItem(new QSpacerItem(10,10,QSizePolicy::Expanding,QSizePolicy::Expanding),2,0,1,2);

   connect( contractButton, SIGNAL(clicked()), this, SLOT(simpleLaplace())	);
   connect( collapseButton, SIGNAL(clicked()), this, SLOT(simpleCollapse()) );
   connect( centerButton,	SIGNAL(clicked()), this, SLOT(simpleCenter())	);
   connect( colorButton,	SIGNAL(clicked()), this, SLOT(simpleColor())	);
   connect( anchorCheck,	SIGNAL(clicked()), this, SLOT(checkAnchorMode()) );
   connect (transfer,	SIGNAL(clicked()),this,SLOT(transferValues()));
   
   QIcon* toolIcon = new QIcon(OpenFlipper::Options::iconDirStr()+OpenFlipper::Options::dirSeparator()+"smoother1.png");
   emit addToolbox( tr("Mesh->Skeleton") , toolBox, toolIcon );
}
void MeshContract::transferValues()
{
	for ( PluginFunctions::ObjectIterator o_it(PluginFunctions::TARGET_OBJECTS) ; o_it != PluginFunctions::objectsEnd(); ++o_it) 
		{
			// This method is applicable only to closed tri meshes
			if ( o_it->dataType( DATA_TRIANGLE_MESH ) )
			{
				ACG::SceneGraph::LineNode*  lineNode = new ACG::SceneGraph::LineNode( ACG::SceneGraph::LineNode::LineSegmentsMode, dynamic_cast < ACG::SceneGraph::SeparatorNode* >( PluginFunctions::getRootNode() ) );
				lineNodes_.push_back(lineNode);
				lineNode->clear();
				TriMesh* mesh = PluginFunctions::triMesh(*o_it);
				
				if(lastPickedVertex.is_valid())
				{
					mesh->property(anchorPoints,lastPickedVertex) = TriMesh::Point(vx->value(),vy->value(),vz->value());
					//mesh->point(lastPickedVertex) = mesh->property(anchorPoints,lastPickedVertex);
					Vec3d v1 = mesh->point(lastPickedVertex);
					Vec3d v2 = mesh->property(anchorPoints,lastPickedVertex);
					lineNode->add_line( v1, v2 );
					TriMesh::Color c = TriMesh::Color(1,0,0,1);
					lineNode->add_color(c);
				}
			}
			emit updatedObject( o_it->id(), UPDATE_ALL );
			emit updateView();
			break;
		}
}

void MeshContract::checkAnchorMode()
{
	 anchorSelectionMode = anchorCheck->isChecked();
	 printf("\nAnchor Check :%d",anchorSelectionMode); 
	 if(anchorSelectionMode)
	 {
		PluginFunctions::actionMode(Viewer::PickingMode); 
		for ( PluginFunctions::ObjectIterator o_it(PluginFunctions::TARGET_OBJECTS) ; o_it != PluginFunctions::objectsEnd(); ++o_it) 
		{
			// This method is applicable only to closed tri meshes
			if ( o_it->dataType( DATA_TRIANGLE_MESH ) )
			{
				TriMesh* mesh = PluginFunctions::triMesh(*o_it);	
				bool propertyexists = mesh->get_property_handle(anchorPoints,"AnchorPoints");
				if(!propertyexists)
				{
					mesh->add_property(anchorPoints,"AnchorPoints");
				}
				TriMesh::VertexIter vh = mesh->vertices_begin(),vh_e = mesh->vertices_end();
				for ( vh; vh!=vh_e; ++vh)
				{
					mesh->set_color(vh,TriMesh::Color(1,1,1,1));
					mesh->property(anchorPoints,vh) = TriMesh::Point(INV,INV,INV);
				}
				o_it->materialNode()->set_point_size(5);
				PluginFunctions::setDrawMode(ACG::SceneGraph::DrawModes::POINTS_COLORED);
				
			}
			emit updatedObject( o_it->id(), UPDATE_ALL );			
			emit updateView();
			break;
		}
	 }
	 else
	 {
		 
		 PluginFunctions::actionMode(Viewer::ExamineMode);
		 PluginFunctions::setDrawMode(ACG::SceneGraph::DrawModes::SOLID_FLAT_SHADED);
		
	 }
}
void MeshContract::slotMouseEvent( QMouseEvent* _event ) {
	//printf(" \n Mouse event");
	if (!anchorSelectionMode)
		return;
	if ( _event->buttons() == Qt::RightButton )
      return;

	if ( _event->type() == QEvent::MouseButtonPress)
	{
		unsigned int        node_idx, target_idx;
		ACG::Vec3d       hit_point;
		if(PluginFunctions::scenegraphPick(ACG::SceneGraph::PICK_VERTEX, _event->pos(),node_idx, target_idx, &hit_point))
		{
			BaseObjectData* object;
			 if ( PluginFunctions::getPickedObject(node_idx, object) ) 
			 {
				 if ( object->picked(node_idx) && object->dataType(DATA_TRIANGLE_MESH)  )
				 {
					TriMesh& m = *PluginFunctions::triMesh(object);
					lastPickedVertex = m.vertex_handle(target_idx);
					lastPickedPos = _event->pos();
					if (lastPickedVertex.is_valid())
					{	m.set_color(lastPickedVertex,TriMesh::Color(1,0,0,1));
						vx->setValue( m.point(lastPickedVertex)[0]);
						vy->setValue( m.point(lastPickedVertex)[1]);
						vz->setValue( m.point(lastPickedVertex)[2]);
					}
					
				 }

				 	emit updatedObject( object->id(), UPDATE_ALL );
					emit updateView();

			 }

		}
		else
		{
			lastPickedVertex = TriMesh::InvalidVertexHandle;
		}
	}
	if ( _event->type() == QEvent::MouseButtonRelease && lastPickedVertex.is_valid() )
	{
		for ( PluginFunctions::ObjectIterator o_it(PluginFunctions::TARGET_OBJECTS) ; o_it != PluginFunctions::objectsEnd(); ++o_it) 
		{
			// This method is applicable only to closed tri meshes
			if ( o_it->dataType( DATA_TRIANGLE_MESH ) )
			{
				TriMesh* mesh = PluginFunctions::triMesh(*o_it);	
				QPoint releasedPos = _event->pos();
				float distance = sqrtf ( (releasedPos.x()  - lastPickedPos.x())*(releasedPos.x()  - lastPickedPos.x()) + (releasedPos.y()  - lastPickedPos.y())*(releasedPos.y()  - lastPickedPos.y()) );
				printf("\n distance : %f pos1(x,y) : (%d,%d) pos2(x,y) : (%d,%d)",distance,lastPickedPos.x(),lastPickedPos.y(),releasedPos.x(),releasedPos.y());
				
				TriMesh::Normal n = mesh->normal(lastPickedVertex);
				//if((releasedPos.x() - lastPickedPos.x())!= 0)
				//double angle = atanf((releasedPos.y() - lastPickedPos.y())/(releasedPos.x() - lastPickedPos.x()));
				TriMesh::Point nPoint = (distance*DEL)*n; // move the point along the normal by distance value
				int sign = (lastPickedPos.x() < releasedPos.x() )? 1: -1;
				nPoint *= sign;
				ACG::SceneGraph::LineNode*  lineNode = new ACG::SceneGraph::LineNode( ACG::SceneGraph::LineNode::LineSegmentsMode, dynamic_cast < ACG::SceneGraph::SeparatorNode* >( PluginFunctions::getRootNode() ) );
				lineNodes_.push_back(lineNode);
				lineNode->clear();
				TriMesh::Point p2 = mesh->point(lastPickedVertex)+nPoint;
				lineNode->add_line(mesh->point(lastPickedVertex),p2);
				lineNode->add_color(TriMesh::Color(0,1,0,1));
				vx->setValue(p2[0]);
				vy->setValue(p2[1]);
				vz->setValue(p2[2]);
				mesh->property(anchorPoints,lastPickedVertex) = TriMesh::Point(vx->value(),vy->value(),vz->value());
  
			}
			
			emit updatedObject( o_it->id(), UPDATE_ALL );
			emit updateView();
			break;
		}
	}
}

void MeshContract::simpleColor()
{
	Sl = spinBoxSl->value();
	TriMesh *mesh;
	PolyLine *line;
	TriMeshObject *meshObj;
	
	for ( PluginFunctions::ObjectIterator o_it(PluginFunctions::TARGET_OBJECTS) ; o_it != PluginFunctions::objectsEnd(); ++o_it) 
	{
		if(o_it->dataType(DATA_TRIANGLE_MESH))
		{
			mesh = PluginFunctions::triMesh(*o_it);
			meshObj =(TriMeshObject*) *o_it;
			OpenMesh::EPropHandleT< double> edgeWeights;
			mesh->add_property(edgeWeights, "edgeWeights");

			// Property - VertexWeights
			OpenMesh::VPropHandleT<double> vertexWeights;
			mesh->add_property(vertexWeights, "vertexWeights");

			// Property - 1/VertexWeights
			OpenMesh::VPropHandleT<double> invVertexWeights;
			mesh->add_property(invVertexWeights, "invVertexWeights");

			OpenMesh::VPropHandleT<TriMesh::Point> deltas;
			mesh->add_property(deltas,"deltas");
			// compute weights assumes that the above properties are defined.

			computeWeights(mesh);

			// compute delta values
			{
				TriMesh::VertexIter vi = mesh->vertices_begin(),vi_e = mesh->vertices_end();
				for(vi; vi != vi_e; ++vi)
				{
					TriMesh::Point n = TriMesh::Point(0,0,0);
					double norm = 0;
					TriMesh::VOHIter vo = mesh->voh_begin(vi.handle()), vo_e = mesh->voh_end(vi.handle());
					for (vo ; vo != vo_e; ++vo)
					{
						EdgeHandle eh = mesh->edge_handle(vo.handle());
						if(eh.is_valid())
						{
							double ew =  1;//mesh->property(edgeWeights,eh); // we didnt do cot/2 in compute weights 
							norm +=ew;
							n += (mesh->point(vi.handle()) - mesh->point(mesh->to_vertex_handle(vo)))*ew;
						}
						
					}
					mesh->property(deltas,vi.handle()) = n / mesh->valence(vi.handle()) ;
				}
			}//delta 

			// Solve the system :
			printf("\nFilling...");			  
			{ // Solve
				
				int matrixSize = mesh->n_vertices();
				// For each co-ordinate
				for(int xyz=0; xyz < 3; xyz++)
				{
					nlNewContext();
					bool check = nlInitExtension("SUPERLU");
					printf("\nsuperlu: %d",check);
					// BICGSTAB since we have a non-symmetric matrix
					nlSolverParameteri(NL_SOLVER,check ? NL_PERM_SUPERLU_EXT:NL_BICGSTAB);
					nlSolverParameterd(NL_THRESHOLD,1e-14);
					nlSolverParameteri(NL_MAX_ITERATIONS, INT_MAX);
					nlSolverParameteri(NL_LEAST_SQUARES, NL_TRUE);
					nlSolverParameteri(NL_NB_VARIABLES, matrixSize);
					nlSolverParameteri(NL_PRECONDITIONER,check?NL_PRECOND_NONE:NL_PRECOND_JACOBI);

					nlBegin(NL_SYSTEM);
					nlBegin(NL_MATRIX);
					int currIdx = 0;
					for(int r = 0; r < (matrixSize); r++)
					{
						int idx = 0;
						if(r < (matrixSize ))
							idx = r;
						else
							idx = r - matrixSize;

						if(currIdx >= mesh->n_vertices())
							currIdx = 0;
						VertexHandle vh = mesh->vertex_handle(idx);
						
						TriMesh::Point p = mesh->point(vh);
						
						// Contraction part of the matrix
						if(r < matrixSize )
						{
							double delta = mesh->property(deltas, vh)[xyz];
							
							nlRowParameterd(NL_RIGHT_HAND_SIDE,delta);
							nlRowParameterd(NL_ROW_SCALING, 1); //Wl contraction -> diagonal weighting matrix..
							nlBegin(NL_ROW);
							nlCoefficient(idx,mesh->valence(vh));//mesh->property(vertexWeights,vh));//
							TriMesh::VertexVertexIter vv = mesh->vv_iter(vh);
						
							for(vv;vv;++vv)
							{
								HalfedgeHandle heh = mesh->find_halfedge(vh,vv.handle());
								EdgeHandle eh = mesh->edge_handle(heh);
								assert(eh.is_valid());
								nlCoefficient(vv.handle().idx(),-1);//-mesh->property(edgeWeights,eh));//-1
							}
							nlEnd(NL_ROW);
						}

					} // rows
				
					//Anchor rows				
					{
						bool atleastOne = false;
						TriMesh::VertexIter vi = mesh->vertices_begin(),vi_e = mesh->vertices_end();
						for (vi ; vi != vi_e; ++vi)
						{
							if(mesh->property(anchorPoints,vi)[0] != INV)
							{
								nlRowParameterd(NL_RIGHT_HAND_SIDE,mesh->property(anchorPoints,vi)[xyz]);
								nlRowParameterd(NL_ROW_SCALING,Sl);
								nlBegin(NL_ROW);
								nlCoefficient(vi.handle().idx(),1);
								nlEnd(NL_ROW);
								atleastOne = true;
							}
						}
						if(!atleastOne)
							printf("\nNo Anchor rows added!!");

					}
					nlEnd(NL_MATRIX);
					nlEnd(NL_SYSTEM);
				  
					// Now we actually solve the matrix
					printf("\nSolving...");
				
					bool finished = nlSolve();
					if(finished)
					{
						int nextVar = 0;
						for(int c = 0; c < mesh->n_vertices(); c++)
						{
							VertexHandle vh = mesh->vertex_handle(c);
							TriMesh::Point p = mesh->point(vh);
							double var;
							if(mesh->property(anchorPoints,vh)[xyz] != INV)
							{
								var = mesh->property(anchorPoints,vh)[xyz];//p[xyz];
							}
							else
							{
								var = nlGetVariable(c);
							}
							p[xyz] = var;
							
							mesh->set_point(vh,p);
						}
					}
					else
					{

						printf("\nCould not solve !@ co-or:%d",xyz);
						break;
					}
					int iters;
					double time;
					nlGetIntergerv(NL_USED_ITERATIONS,&iters);
					nlGetDoublev(NL_ELAPSED_TIME,&time);
					printf("\nNo. of iterations : %d",iters);
					printf("\nElapsed time: %f", time);
					nlDeleteContext(nlGetCurrent());
			 
				} // co-ordinates
			} // solve
	
			
			  printf("\n ********");
			  // clear selected points and line nodes
			  TriMesh::VertexIter vi = mesh->vertices_begin(),vi_e = mesh->vertices_end();
			  for(vi; vi!=vi_e; ++vi)
			  {
				  mesh->property(anchorPoints,vi.handle()) = TriMesh::Point(INV,INV,INV);
			  }
			  int i=0;
			  while(!lineNodes_.empty()){
				  ACG::SceneGraph::LineNode *l = lineNodes_[lineNodes_.size()-1];
				  lineNodes_.pop_back();
				  delete l;
			  }
			mesh->remove_property(edgeWeights);
			mesh->remove_property(vertexWeights);
			mesh->remove_property(invVertexWeights);
		}
		break;
	}	
	Vec3d bbmin,bbmax;//zoom to objects
	meshObj->getBoundingBox(bbmin,bbmax);
    ACG::Vec3d bbcenter = (bbmax + bbmin) * 0.5;

    double bbradius = (bbmax - bbmin).norm();

    ACG::Vec3d eye = bbcenter + (PluginFunctions::eyePos() - bbcenter).normalize() * bbradius ;

    PluginFunctions::flyTo(eye, bbcenter );
	emit updatedObject( meshObj->id(), UPDATE_ALL );
	emit createBackup(meshObj->id(), "Color", UPDATE_ALL );
	emit updateView();
}


void MeshContract::pluginsInitialized() {
    
    // Emit slot description
    emit setSlotDescription(tr("simpleLaplace(int)"),   tr("Smooth mesh using the Laplace operator with uniform weights."),
                            QStringList(tr("iterations")), QStringList(tr("Number of iterations")));

}

/*
* Sum-area-all-model-faces/no-of-model-faces
* For the same object, model with higher triangle count has lower avg face area
* Use this to configure initial contraction factor
*/
double MeshContract::findAvgFaceArea(TriMesh *mesh)
{
	double area = 0;
	TriMesh::FaceIter fIter, fiter_end = mesh->faces_end();
	
	for(fIter = mesh->faces_begin(); fIter != fiter_end; ++fIter)
	{
		TriMesh::Face f = mesh->face(fIter);
		TriMesh::FaceHandle h = fIter.handle();
		area +=	faceArea(mesh,h);
	}
	return (area/mesh->n_faces());
	
}


/*
* Area of all faces incident on vertex vh
* Use this to see "how much" contraction has taken place already
* Scale attraction factor accordingly
*/
double MeshContract::findOneRingArea(TriMesh *mesh, TriMesh::VertexHandle vh)
{
	TriMesh::VertexFaceIter vfIter, vfiter_end = mesh->vf_end(vh);
	VPropHandleT<double> attractionWeights;
	bool gotWeights = mesh->get_property_handle(attractionWeights,"attractionWeight");
	
	VPropHandleT<double> contractionWeights;
	gotWeights = mesh->get_property_handle(contractionWeights,"contractionWeight");
	VPropHandleT<bool> isDegenerate;
	bool gotPropHandle = mesh->get_property_handle(isDegenerate,"isDegenerate");
	VPropHandleT<int> neighbourCount;
	mesh->get_property_handle(neighbourCount,"neighbourCount");
	if(!gotPropHandle)
		printf("\nisDegenerate handle missing ");

	double area = 0;
	int isDeg = 0;	
	for(vfIter = mesh->vf_begin(vh); vfIter != vfiter_end; ++vfIter)
	{
		double currArea = faceArea(mesh,vfIter.handle());
		area += currArea;
		if(currArea < avgArea/100)
		{
			isDeg++;
		}
	}
	return area;
}

/*
* Volume enclosed by mesh
* Terminate iterations when volume < 1e-6
*/
double MeshContract::findMeshVolume(TriMesh *mesh)
{
	return 1.0;
}
// Slots

/** \brief simpleCenter
 *
 *  Centers skeleton nodes within model
 *  Merge nodes? 
 */

void MeshContract::simpleCenter()
{
	emit log(LOGWARN, "Simple Center not yet implemented\n");
}

bool	MeshContract::okToCollapse(TriMesh *mesh, HalfedgeHandle heh)
{
	
	// heh(i->j) if k is common vertex to i and j, ijk must be a face
	if( !heh.is_valid() || mesh->status(heh).deleted() || heh.idx() < 0)
		return false;

	if(mesh->prev_halfedge_handle(heh) == mesh->next_halfedge_handle(heh))
		return false;
	
	VertexHandle i = mesh->from_vertex_handle(heh);	
	VertexHandle j = mesh->to_vertex_handle(heh);
	if( i == j)
		return false;
	HalfedgeHandle prev = mesh->prev_halfedge_handle(heh);
	HalfedgeHandle next = mesh->next_halfedge_handle(heh);
	HalfedgeHandle opp = mesh->opposite_halfedge_handle(heh);
	if(next == prev)
		return false;
	if(mesh->next_halfedge_handle(opp) == mesh->prev_halfedge_handle(opp))
		return false;
	if(next == opp)
		return false;
	if(prev == opp)
		return false;

	// either  from->to->next_to or opp-from->opp-to->opp-next-to must be a face else what are we collapsing
	VertexHandle k = mesh->to_vertex_handle(next);
	//i-j-k
	bool faceOne = true, faceTwo = true;
	if(!i.is_valid() || !j.is_valid() || !k.is_valid())
		faceOne = false;
	if(mesh->status(i).deleted() || mesh->status(j).deleted() || mesh->status(k).deleted())
		faceOne = false;

	if(i==j || j==k || k== i)
		faceOne = false;

	VertexHandle x = mesh->from_vertex_handle(opp);
	VertexHandle y = mesh->to_vertex_handle(opp);
	VertexHandle z = mesh->to_vertex_handle(mesh->next_halfedge_handle(opp));

	if(!x.is_valid() || !y.is_valid() || !z.is_valid())
		faceTwo = false;

	if(mesh->status(x).deleted() || mesh->status(y).deleted() || mesh->status(z).deleted())
		faceTwo = false;
	if(x==y || y==z || z==x)
		faceTwo = false;

	if(!faceOne && !faceTwo)
		return false;

	return true;
	
	std::set<VertexHandle> i_set;
	std::set<VertexHandle> j_set; // to detect looping
	TriMesh::VertexVertexIter vv = mesh->vv_begin(i), vv_e = mesh->vv_end(i);
	HalfedgeHandle h1 = vv.current_halfedge_handle();
	
	if(!h1.is_valid() || vv.handle().idx() == -1 )
		return false;

	if(!vv.handle().is_valid())
		return false;
	
	for(vv; vv!=vv_e; ++vv)
	{
		VertexHandle k = vv.handle();
		std::set<VertexHandle>::iterator it = i_set.find(k);
		if(it != i_set.end())
		{
			//printf("\nWARN: LOOPING IN VVITER(i) - FORCE BREAK");
			break;
			return true;
		}
		if(vv.handle().is_valid() && !mesh->status(vv.handle()).deleted())
			i_set.insert(vv.handle());
	}

	TriMesh::VertexVertexIter vvj = mesh->vv_begin(j), vvj_e = mesh->vv_end(j);
	HalfedgeHandle h = vvj.current_halfedge_handle();

	if(!h.is_valid() || vvj.handle().idx() == -1 )
		return false;
	for(vvj; vvj!=vvj_e; ++vvj)
	{
		VertexHandle k = vvj.handle();
		if((k.idx() == -1)||!k.is_valid() || mesh->status(k).deleted())
			continue;
		std::set<VertexHandle>::iterator loop = j_set.find(k);
		if(loop != j_set.end())
		{
			//printf("\nWARN: LOOPING IN VVITER(j) - FORCE BREAK");
			return false;
		}
		
		j_set.insert(k);
		std::set<VertexHandle>::iterator it = i_set.find(k);
		if(it != i_set.end())
		{	
			// <i,j,k >in mesh ? 
			if (k != mesh->opposite_vh(heh) && k != mesh->opposite_he_opposite_vh(heh))
				return false;
		}
	}
	return true; //mesh->is_collapse_ok(heh);
}

int		MeshContract::doCollapse(TriMesh *mesh, HalfedgeHandle heh)
{
	VertexHandle from = mesh->from_vertex_handle(heh);
	VertexHandle to = mesh->to_vertex_handle(heh);
	TriMesh::Point newPoint = mesh->point(from)*0.5 + mesh->point(to)*0.5;

	mesh->collapse(heh);
	mesh->point(to) = newPoint;
	return 1;
}

/** \brief simpleCollapse
 *
 *  Edge collapses mesh using the QEM 
 *  to generate 1-d skeleton
 */
// Note QEM doesn't give the results described
// I found it easier to simply collapse the shortest edge greedily

void MeshContract::simpleCollapse()
{
	printf("\n *** Collapsing ***");
	// clear up any old lines
	while(!lineNodes_.empty()){
			ACG::SceneGraph::LineNode *l = lineNodes_[lineNodes_.size()-1];
			lineNodes_.pop_back();
			delete l;
		}
	for ( PluginFunctions::ObjectIterator o_it(PluginFunctions::TARGET_OBJECTS) ; o_it != PluginFunctions::objectsEnd(); ++o_it) 
	{
		// This method is applicable only to closed tri meshes
		if ( o_it->dataType( DATA_TRIANGLE_MESH ) )
		{
			PluginFunctions::setDrawMode(ACG::SceneGraph::DrawModes::EDGES_COLORED);
			// Get the mesh to work on
			TriMesh* origmesh = PluginFunctions::triMesh(*o_it);
			TriMesh  meshCopy;
			meshCopy = *origmesh;
			TriMesh *mesh = &meshCopy;
			origmesh->request_edge_colors();
			mesh->request_edge_colors();
			//color all edges
			TriMesh::EdgeIter ei;
			for(ei = origmesh->edges_begin();ei !=origmesh->edges_end();++ei)
			{
				origmesh->set_color(ei,TriMesh::Color(0,0,1,1));
			}
			
			mesh->request_edge_status();
			mesh->request_face_status();
			mesh->request_vertex_status();
			

			EPropHandleT<double> edgePri;
			HPropHandleT<int> edgePos;
			VPropHandleT<OpenMesh::Geometry::Quadricd> vertexQEM;
			mesh->add_property(edgePri,"edgePri");
			mesh->add_property(edgePos,"edgePos");
			mesh->add_property(vertexQEM,"vertexQEM");

			TriMesh::EdgeIter ei_ = mesh->edges_begin(),ei_e=mesh->edges_end();
			for(ei_; ei_!=ei_e; ++ei_)
			{
				mesh->property(edgePri,ei_.handle()) = mesh->calc_edge_length(ei_.handle());	
			}

			int collapse_target = iterationsSpinbox_->value() ;
			int n_collapses = 0;
			while( n_collapses < collapse_target)
			{
				
				EdgeHandle currentEdge = TriMesh::InvalidEdgeHandle;
				EdgeHandle ei = TriMesh::InvalidEdgeHandle;
				double minPriority = DBL_MAX;
				int numEdges = mesh->n_edges();
				for(uint e=0; e < numEdges; ++e)
				{
					ei = mesh->edge_handle(e);
					if(ei.idx() < 0 || mesh->to_vertex_handle(mesh->halfedge_handle(ei,0)) == mesh->from_vertex_handle(mesh->halfedge_handle(ei,0)) )
						mesh->status(ei).set_tagged2(true);
					if(mesh->property(edgePri,ei) < minPriority && (!mesh->status(ei).deleted() && !mesh->status(ei).tagged2()) &&
						okToCollapse(mesh,mesh->halfedge_handle(ei,0)))
						
					{
						currentEdge = ei;
						minPriority = mesh->property(edgePri,currentEdge);
					}

				}

				if((currentEdge.idx() < 0) || mesh->status(currentEdge).deleted() || mesh->status(currentEdge).tagged2() || !(okToCollapse(mesh,mesh->halfedge_handle(currentEdge,0))))
				{

					//TriMesh::StatusInfo stat = mesh->status(currentEdge);
					//printf("\nbeh %d d=%d t2=%d",currentEdge.idx(),stat.deleted(),stat.tagged2());
					printf("\nCannot find suitable edge");
					break;
				}
				
				if(okToCollapse(mesh,mesh->halfedge_handle(currentEdge,0)))
				{
					VertexHandle currentVertex = mesh->to_vertex_handle(mesh->halfedge_handle(currentEdge,0));					
					doCollapse(mesh,mesh->halfedge_handle(currentEdge,0));
					n_collapses++;

					TriMesh::VertexEdgeIter vei = mesh->ve_iter(currentVertex), vei_end = mesh->ve_end(currentVertex);
					for(vei ; vei.handle().idx() > 0, vei!=vei_end; ++vei)
					{
						mesh->property(edgePri,vei.handle()) += mesh->calc_edge_length(vei.handle());
					}
					} // ok to collapse, do collapse
			
				} // while n collapses							
				printf("\nDid %d collapses",n_collapses);
				int nfaces= mesh->n_faces(); int cfaces = 0;
				for(int i=0; i< nfaces; i++)
				{
					TriMesh::FaceHandle fh = mesh->face_handle(i);
					if(!mesh->status(fh).deleted())
					{
						cfaces++;
					}
				}
				printf("\nno. of faces : %d",cfaces);

				mesh->remove_property(edgePri);
				mesh->remove_property(vertexQEM);
				mesh->remove_property(edgePos);
			
				// now just add lines 
				for(int i=0; i < mesh->n_edges();i++)
				{
					ACG::SceneGraph::LineNode*  lineNode = new ACG::SceneGraph::LineNode( ACG::SceneGraph::LineNode::LineSegmentsMode, dynamic_cast < ACG::SceneGraph::SeparatorNode* >( PluginFunctions::getRootNode() ) );
					lineNodes_.push_back(lineNode);
					lineNode->clear();
					EdgeHandle eh_ = mesh->edge_handle(i);
					if(!eh_.is_valid() || mesh->status(eh_).deleted())
						continue;
					HalfedgeHandle heh_ = mesh->halfedge_handle(eh_,0);
					Vec3d v1 = mesh->point(mesh->to_vertex_handle(heh_));
					Vec3d v2 = mesh->point(mesh->from_vertex_handle(heh_));
					lineNode->add_line(v1,v2);
					lineNode->add_color(TriMesh::Color(0,1,0,1));
				}
  
			} // triangle_mesh
		emit updatedObject( o_it->id(), UPDATE_ALL );
		emit updateView();

		break; // only for the first object		
			
		}	
	}



/** \brief simpleLaplace
 *
 *  Smooth mesh using the Laplace operator
 *  with uniform weights.
 */
void MeshContract::simpleLaplace() {

    int iterations = 1;
    
    if(!OpenFlipper::Options::nogui()) {
        iterations = iterationsSpinbox_->value();
		Wh0 = spinBoxWh0->value();
		Wl0 = spinBoxWl0->value();
		Sl = spinBoxSl->value();
    }
    
    simpleLaplace(iterations);
}


/*
* return area of face fh
*/
double MeshContract::faceArea(TriMesh *mesh, TriMesh::FaceHandle fh)
{
	TriMesh::FaceVertexIter fvit,fvit_e = mesh->fv_end(fh);
	TriMesh::VertexHandle vh[3];
	int i = 0;

	for(fvit = mesh->fv_begin(fh);fvit != fvit_e; ++fvit)
	{
		vh[i++] = fvit.handle();
	}
	assert(i==3);	// Triangular models only

	TriMesh::Point p0 = mesh->point(vh[0]);
	TriMesh::Point p1 = mesh->point(vh[1]);
	TriMesh::Point p2 = mesh->point(vh[2]);

	OpenMesh::Vec3d v1 = p1-p0; // Don't normalize! we want the area
	OpenMesh::Vec3d v2 = p2-p0; 

	double	area = 0.5*cross(v1,v2).length();
	return area;
}

/*
* check for degenerate cases
*/

void MeshContract::handleDegenerate(TriMesh *mesh)
{
	// Go through all the vertices in this mesh
	// Check its area, and weights
	// If something is too high, just lock it

	VPropHandleT<bool> isDegenerate;
	VPropHandleT<double> vertexWeights;
	VPropHandleT<double> attractionWeights;
	VPropHandleT<double> attrWeightsTemp;
	VPropHandleT<double> oneRing;
	mesh->add_property(attrWeightsTemp,"attrTemp");
	bool success = false;
	success = mesh->get_property_handle(isDegenerate,"isDegenerate");

	if (success)
		success = mesh->get_property_handle(vertexWeights, "vertexWeights");
	if (success)
		success = mesh->get_property_handle(attractionWeights,"attractionWeight");
	if (success)
		success = mesh->get_property_handle(oneRing,"SmoothingPlugin_Original_OneRing");
	if(!success)
		printf("\n could not get necessary handles");
		
	//avgArea is the average vertex area

	//Check for each vertex
	TriMesh::VertexIter viter = mesh->vertices_begin(), viter_end = mesh->vertices_end();
	for(viter; viter != viter_end; ++viter)
	{

		TriMesh::VertexFaceIter vf = mesh->vf_begin(viter.handle()), vf_end = mesh->vf_end(viter.handle());
		int count = 0;
		int num = 0;
		{
			if (findOneRingArea(mesh,viter.handle()) < (mesh->property(oneRing,viter.handle())*0.001))
			
			{
				
			mesh->property(isDegenerate,viter.handle()) = true;
			TriMesh::VertexVertexIter vv = mesh->vv_begin(viter.handle()), vve = mesh->vv_end(viter.handle());
			
			mesh->set_color(viter.handle(),TriMesh::Color(0,0,1,1));
			

			}
		
				
		}
		
	}
	mesh->remove_property(attrWeightsTemp);

}


/*
* for all Edges of mesh, compute the cotangent weight cot(a)+cot(b)
* for each edge e(v0,v1) 
* set weight(e)   = cot(a)+cot(b)
*	  weight(v0) += cot(a)+cot(b)
*	  weight(v1) += cot(a)+cot(b)
*
* if(cot(a)+cot(b) < threshold) set weight=threshold
* Notes: 1/tan(angle) angle = acos(dot) dot->0.001->0.99 has been the best shot yet..
*/

void MeshContract::computeWeights(TriMesh *mesh)
{
	double weight = 0;
	EPropHandleT<double> edgeWeights;
	VPropHandleT<double> vertexWeights, invVertexWeights;
	bool success = false;
	success = mesh->get_property_handle(edgeWeights,"edgeWeights");
	if(success)
		success = mesh->get_property_handle(vertexWeights, "vertexWeights");
	if(success)
		success = mesh->get_property_handle(invVertexWeights, "invVertexWeights");

	if(!success)
		printf("\n could not get necessary handles");
	// Clear the vertex Weights per iteration
	TriMesh::VertexIter vertex_iter, vertex_iter_end= mesh->vertices_end();
	for (vertex_iter=mesh->vertices_begin(); vertex_iter!=vertex_iter_end; ++vertex_iter)
	{
		mesh->property(vertexWeights, vertex_iter) = 0;
		mesh->property(invVertexWeights, vertex_iter) = 0;
		mesh->set_color(vertex_iter,TriMesh::Color(0.5,0.5,0.5,1));


			
		TriMesh::VertexEdgeIter ve = mesh->ve_begin(vertex_iter.handle()), ve_end = mesh->ve_end(vertex_iter.handle());
		for(ve; ve!=ve_end; ++ve)
		{
			mesh->property(edgeWeights,ve.handle())=0;
			
		}
	}

	// Edge Weights
	TriMesh::EdgeIter edge_iter, edge_iter_end = mesh->edges_end();
	for (edge_iter=mesh->edges_begin(); edge_iter != edge_iter_end; ++edge_iter)
	{
		TriMesh::HalfedgeHandle heh0 = mesh->halfedge_handle(edge_iter.handle(),0);
		TriMesh::VertexHandle v0 = mesh->to_vertex_handle(heh0);
		TriMesh::Point p0 = mesh->point(v0);

		TriMesh::HalfedgeHandle heh1 = mesh->halfedge_handle(edge_iter.handle(),1);
		TriMesh::VertexHandle v1 = mesh->to_vertex_handle(heh1);
		TriMesh::Point p1 = mesh->point(v1);

		//if(mesh->property(isDegenerate,v0) && mesh->property(isDegenerate,v1))
		//	continue;
		TriMesh::HalfedgeHandle heh2 = mesh->next_halfedge_handle(heh0);
		TriMesh::Point p2 = mesh->point(mesh->to_vertex_handle(heh2));

		OpenMesh::Vec3d d0 = p0-p2; d0.normalize();
		OpenMesh::Vec3d d1 = p1-p2; d1.normalize();

		double weight1 = 1.0/(tan(acos(std::max(-BOUNDS,std::min(BOUNDS,dot(d0,d1))))));
		double cross1 = cross(d0,d1).length();
		double dot1 = dot(d0,d1);
		double angle1 = acos(dot1);//( acos(std::max(0.01,std::min(dot1,0.99))));
		heh2 = mesh->next_halfedge_handle(heh1);
		p2 = mesh->point(mesh->to_vertex_handle(heh2));
		d0 = p0-p2; d0.normalize();
		d1 = p1-p2; d1.normalize();
		double weight2 = 1.0/(tan(acos(std::max(-BOUNDS,std::min(BOUNDS,dot(d0,d1))))));		
		double cross2 = cross(d0,d1).length();
		double dot2 = dot(d0,d1);
		double angle2 = acos(dot2);//( acos(std::max(0.01,std::min(dot2,0.99))));
		double test = (dot2/cross2) + (dot1/cross1);
		
		// we want the angles between 0- PI/2 or 0- 90
		
		angle2 = rad_to_deg(angle2);
		angle2 = std::max(1.0,std::min(angle2,89.0));
		angle2 = deg_to_rad(angle2);
		
		double cotwt =  (weight1) + (weight2);//((0.5/tan(angle2)))+((0.5/tan(angle1)));  
						cotwt = std::max(-EDGEBOUNDS,std::min(cotwt,EDGEBOUNDS));
		double color = cotwt*mesh->calc_edge_length(edge_iter);
		mesh->property(invVertexWeights,v0)+=color;
		mesh->property(invVertexWeights,v1)+=color;
		mesh->property(edgeWeights, edge_iter) = cotwt;
		mesh->property(vertexWeights, v0) += cotwt;
		mesh->property(vertexWeights, v1) += cotwt;
		
	}
	
	// Inverse Vertex Weights
	for (vertex_iter=mesh->vertices_begin(); vertex_iter!=vertex_iter_end; ++vertex_iter)
	{
		double color = mesh->property(invVertexWeights,vertex_iter);
		weight = mesh->property(vertexWeights, vertex_iter);
	}
}
/** \brief simpleLaplace
 *
 * Smooth mesh using the Laplace operator
 * with uniform weights.
 *
 * @param _iterations Number of iterations
 */

void MeshContract::simpleLaplace(int _iterations) {
    
    for ( PluginFunctions::ObjectIterator o_it(PluginFunctions::TARGET_OBJECTS) ; o_it != PluginFunctions::objectsEnd(); ++o_it) 
	{
		// This method is applicable only to closed tri meshes
		if ( o_it->dataType( DATA_TRIANGLE_MESH ) )
		{
			// Get the mesh to work on
			TriMesh* mesh = PluginFunctions::triMesh(*o_it);
			// Property - EdgeWeights
			OpenMesh::EPropHandleT< double> edgeWeights;
			mesh->add_property(edgeWeights, "edgeWeights");

			// Property - VertexWeights
			OpenMesh::VPropHandleT<double> vertexWeights;
			mesh->add_property(vertexWeights, "vertexWeights");

			// Property - 1/VertexWeights
			OpenMesh::VPropHandleT<double> invVertexWeights;
			mesh->add_property(invVertexWeights, "invVertexWeights");

			// Property - One-Ring area
			OpenMesh::VPropHandleT<double> oneRingArea;
			mesh->add_property(oneRingArea,"MeshContract_One_Ring_Area");

			// Property - One-Ring area  (original)
			OpenMesh::VPropHandleT<double> originalOneRing;
			mesh->add_property(originalOneRing,"SmoothingPlugin_Original_OneRing");

			// Property - Contraction Weight
			OpenMesh::VPropHandleT<double> contractionWeight;
			mesh->add_property(contractionWeight,"contractionWeight");
			
			// Property - Attraction Weight
			OpenMesh::VPropHandleT<double> attractionWeight;
			mesh->add_property(attractionWeight,"attractionWeight");


			// Property - Original points
			OpenMesh::VPropHandleT< TriMesh::Point > origPositions;
			mesh->add_property( origPositions, "MeshContract_Original_Positions" );

			// Property - Degenerate points
			OpenMesh::VPropHandleT<bool> isDegenerate;
			mesh->add_property(isDegenerate, "isDegenerate");

			OpenMesh::VPropHandleT<int> newIdx;
			mesh->add_property(newIdx,"newIdx");

			// ITERATIONS
			for ( int i = 0 ; i < _iterations ; ++i )
			{
		  
			  // Find the average face area of the mesh
			  avgArea = findAvgFaceArea(mesh);
			  printf("avgarea: %f\n",avgArea);
			  printf("sqrtavgarea: %f\n",sqrt(avgArea));

			  // COMPUTE COTANGENT WEIGHTS FOR THE MESH
			  computeWeights(mesh);

			  TriMesh::VertexIter v_it, v_end=mesh->vertices_end();
			  
			  // FOR EACH VERTEX, PER ITERATION
			  for (v_it=mesh->vertices_begin(); v_it!=v_end; ++v_it) 
			  {
			   
				  // Copy the original postions and reset all degenerate vertices 
				  if(i==0)
				  { 
					  mesh->property(origPositions,v_it) = mesh->point(v_it);
					  mesh->property(isDegenerate,v_it) = false;
				  }
			
				//Find the one ring area for each vertex and update the property
				double onering = findOneRingArea(mesh,v_it);
				mesh->property(oneRingArea,v_it) = onering;
				
				if(i==0)
				{
					// update the original one-ring-area property
					mesh->property(originalOneRing,v_it) = onering;
				}

				
				if(i==0)
				{
					mesh->property(attractionWeight,v_it) = Wh0;
					mesh->property(contractionWeight,v_it) = Wl0;//std::max(sqrt(avgArea), Wl0);
		
				}
				else
				{
				
					double origArea = (mesh->property(originalOneRing,v_it.handle()));
					double currArea = (mesh->property(oneRingArea,v_it.handle()));
					Vec3d diff = mesh->property(origPositions,v_it.handle()) - mesh->point(v_it.handle());
					//mesh->property(origPositions,v_it.handle()) = mesh->point(v_it.handle());
					double dist = diff.length();
					double neww = Wh0* sqrt((origArea/currArea));//*mesh->property(vertexWeights,v_it.handle());
					//neww = std::min(neww,40.0);	
					double cont = mesh->property(contractionWeight,v_it.handle())*Sl;
					mesh->property(attractionWeight,v_it.handle()) = neww;
					mesh->property(contractionWeight,v_it.handle()) = cont;
				}

			 }// FOR - EACH VERTEX

			//handleDegenerate(mesh);
			// We have computed the following:
			// 1. Laplacian weights for all edges and vertices (compute weights)
			// 2. Attraction and contraction constraints for all vertices
			// Now fill the sparse matrix and ask openNL to solve it
			printf("\nFilling...");			  
			{ // Solve
				
				int matrixSize = mesh->n_vertices();
				TriMesh::Point minPoint, maxPoint;
				minPoint = TriMesh::Point(DBL_MAX,DBL_MAX,DBL_MAX);
				maxPoint = TriMesh::Point(-DBL_MAX, - DBL_MAX, - DBL_MAX);
				// count locked points
				int numLocked = 0;
				int newVertexIdx = 0;
				for(int v = 0; v < mesh->n_vertices(); v++)
				{
					VertexHandle vh = mesh->vertex_handle(v);
					mesh->property(newIdx,vh) = vh.idx();
				}
				assert(numLocked < mesh->n_vertices());
				matrixSize = mesh->n_vertices() - numLocked;
				printf("\n%d Locked vertices",numLocked);
				// For each co-ordinate
				for(int xyz=0; xyz < 3; xyz++)
				{
					nlNewContext();
					bool check = nlInitExtension("SUPERLU");
					printf("\nsuperlu: %d",check);
					// BICGSTAB since we have a non-symmetric matrix
					nlSolverParameteri(NL_SOLVER,check ? NL_PERM_SUPERLU_EXT:NL_BICGSTAB);
					nlSolverParameterd(NL_THRESHOLD,1e-14);
					nlSolverParameteri(NL_MAX_ITERATIONS, INT_MAX);
					nlSolverParameteri(NL_LEAST_SQUARES, NL_TRUE);
					nlSolverParameteri(NL_NB_VARIABLES, matrixSize);
					nlSolverParameteri(NL_PRECONDITIONER,check?NL_PRECOND_NONE:NL_PRECOND_JACOBI);

					nlBegin(NL_SYSTEM);
					nlBegin(NL_MATRIX);
					int currIdx = 0;
					for(int r = 0; r < 2*(matrixSize); r++)
					{
					  
						
						int idx = 0;
						if(r < (matrixSize ))
							idx = r;
						else
							idx = r - matrixSize;

						if(currIdx >= mesh->n_vertices())
							currIdx = 0;
						VertexHandle vh = mesh->vertex_handle(idx);
						TriMesh::Point p = mesh->point(vh);
						
						// Contraction part of the matrix
						if(r < matrixSize )
						{
						  
							double cont = mesh->property(contractionWeight,vh);
							if(mesh->property(isDegenerate, vh)) 
								cont = 0;
							assert(cont >= 0);
							//nlRowParameteri(NL_NORMALIZE_ROWS,NL_TRUE);
							nlRowParameterd(NL_ROW_SCALING, cont); //Wl contraction -> diagonal weighting matrix..
							nlRowParameterd(NL_RIGHT_HAND_SIDE, mesh->property(isDegenerate, vh)? p[xyz]:0);
						  
							
							nlBegin(NL_ROW);
							//assert( idx == (vh.idx()));

							nlCoefficient(idx,-mesh->property(vertexWeights,vh));

							TriMesh::VertexVertexIter vv = mesh->vv_iter(vh);
						
							for(vv;vv;++vv)
							{
								HalfedgeHandle heh = mesh->find_halfedge(vh,vv.handle());
								EdgeHandle eh = mesh->edge_handle(heh);
								assert(eh.is_valid());
								nlCoefficient(vv.handle().idx(),mesh->property(edgeWeights,eh));
							  
							}
							nlEnd(NL_ROW);
						}

						// Attraction part of the matrix
						else
						{
							double attr = mesh->property(attractionWeight,vh);
							assert(attr >= 0);
							if(mesh->property(isDegenerate, vh)) 
								attr = 1;
							nlRowParameterd(NL_ROW_SCALING, attr); // Wh attraction, for attr it doesn't matter
							nlRowParameterd(NL_RIGHT_HAND_SIDE,p[xyz]); //attract to current pos
							nlBegin(NL_ROW);
							nlCoefficient(idx,1); // Identity matrix basically, rest of the cols are zero..
							nlEnd(NL_ROW);
						
						}
					  
					
					} // rows
				  
					nlEnd(NL_MATRIX);
					nlEnd(NL_SYSTEM);
				  
					// Now we actually solve the matrix
					printf("\nSolving...");
					bool finished = nlSolve();
					if(finished)
					{
						int nextVar = 0;
						for(int c = 0; c < mesh->n_vertices(); c++)
						{
							VertexHandle vh = mesh->vertex_handle(c);
							TriMesh::Point p = mesh->point(vh);
							double var;
							{
								var = nlGetVariable(c);
							}
							p[xyz] = var;
							if(minPoint[xyz] > p[xyz])
							{
								minPoint[xyz] = p[xyz];
							}
							if(maxPoint[xyz] < p[xyz])
							{
								maxPoint[xyz] = p[xyz];
							}
							mesh->set_point(vh,p);
						}
					}
					else
					{
						printf("\nCould not solve !@ Iter: %d co-or:%d",i,xyz);
						break;
					}
					int iters;
					double time;
					nlGetIntergerv(NL_USED_ITERATIONS,&iters);
					nlGetDoublev(NL_ELAPSED_TIME,&time);
					printf("\nNo. of iterations : %d",iters);
					printf("\nElapsed time: %f", time);
					nlDeleteContext(nlGetCurrent());
			 
				} // co-ordinates
			  
			} // solve
		  	mesh->update_normals();
	
			emit updatedObject( o_it->id(), UPDATE_ALL );
			// Create backup
			emit createBackup(o_it->id(), "Last Iteration", UPDATE_ALL );
			o_it->update();
			emit updateView();

			emit UpdateWindow(NULL);


		  }// Iterations end

		  // Remove the property
		  mesh->remove_property( origPositions );
		  mesh->remove_property( edgeWeights);
		  mesh->remove_property( oneRingArea);
		  mesh->remove_property( vertexWeights);
		  mesh->remove_property( invVertexWeights);
		  mesh->remove_property( originalOneRing);
		  mesh->remove_property( contractionWeight);
		  mesh->remove_property( attractionWeight);
		  mesh->remove_property(isDegenerate);
		  mesh->remove_property(newIdx);
		  mesh->update_normals();
	   }
   
		else {
		  emit log(LOGERR, "DataType not supported.");
		}

		break; //only doing for the first object : D
  }
  
  // Show script logging
  emit scriptInfo("simpleLaplace(" + QString::number(_iterations) + ")");  
  emit updateView();
}


Q_EXPORT_PLUGIN2( MeshContract , MeshContract );

